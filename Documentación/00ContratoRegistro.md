![Help Builder Web Site](./Logo.png)
# Proyecto_SA_Grupo2
### Modulo "Registro de usuarios en iVoting"

**ID: SA-000**
<br>
**Nombre: Registro de usuarios**


<table>
<thead>
	<tr>
		<th>Historia de usuario</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Como administrador del sistema
Quiero un servicio REST
Para poder registrar a los usuarios en el sistema
</td>
	</tr>
</tbody>
</table>




<table>
<thead>
	<tr>
		<th>Prioridad</th>
		<th>Estimado</th>
		<th>Modulo</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Alta</td>
		<td>10 puntos</td>
		<td>Registro</td>
	</tr>
</tbody>
</table>



**Criterios de Aceptación**
<br>
El sistema debe asegurar que una persona solo pueda registrarse una vez, y asegurar que la persona que ingresa los datos sea la persona que dice ser.

El servicio debe tener la siguiente configuración:

<br>
<table>
<tbody>
	<tr>
	<td>Ruta</td>
	<td>/api/registrar</td>
	</tr>
	<tr>
	<td>Método</td>
	<td>POST</td>
	</tr>
	<tr>
	<td>Formato de entrada</td>
	<td>JSON</td>
	</tr>
</tbody>
</table>

<br>

**Header**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Content-type</td>
		<td>header</td>
		<td>application/json</td>
	</tr>
	<tr>
		<td>Authentication</td>
		<td>header</td>
		<td>token <TOKEN></td>
	</tr>
</tbody>
</table>

<br>

**Parametros de Entrada**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>cui</td> 
		<td>Cadena</td>
		<td>
Código único de identificación de una persona.
</td>
	</tr>
	<tr>
		<td>email</td> 
		<td>Cadena</td>
		<td>
Correo electrónico del usuario.
</td>
	</tr>
	<tr>
		<td>teléfono</td> 
		<td>Entero</td>
		<td>
Número de teléfono del usuario.
</td>
	</tr>
	<tr>
		<td>foto_per</td> 
		<td>Cadena</td>
		<td>Ruta de fotografía del usuario.
		</td>
	</tr>
	<tr>
		<td>foto_dpi</td> 
		<td>Cadena</td>
		<td>Ruta de la fotografía del DPI del usuario.
		</td>
	</tr>
	<tr>
		<td>pin</td> 
		<td>Entero</td>
		<td>Código de 6 dígitos del usuario.
		</td>
	</tr>
</tbody>
</table>
<br>



<table>
<tbody>
	<tr>
	<td>Formato de Salida</td>
	<td>JSON</td>
	</tr>
	<tr>
	<td>Código de respuesta exitosa</td>
	<td>HTTP 200</td>
	</tr>
</tbody>
</table>

**Parametros de Salida Exitosa**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>id</td> 
		<td>Entero</td>
		<td>
Código del usuario registrado en el sistema.
</td>
	</tr>
		<tr>
		<td>cui</td> 
		<td>Cadena</td>
		<td>Código único de identificación de una persona.</td>
	</tr>
	<tr>
		<td>email</td> 
		<td>Cadena</td>
		<td>Correo electrónico del usuario.
		</td>
	</tr>
	<tr>
		<td>teléfono</td> 
		<td>Entero</td>
		<td> Número de teléfono del usuario.
		</td>
	</tr>
	<tr>
		<td>foto_per</td> 
		<td>Cadena</td>
		<td>Ruta de fotografía del usuario.
		</td>
	</tr>
	<tr>
		<td>foto_dpi</td> 
		<td>Cadena</td>
		<td>Ruta de la fotografía del DPI del usuario.
		</td>
	</tr>
	<tr>
		<td>pin</td> 
		<td>Entero</td>
		<td>Código de 6 dígitos del usuario.</td>
	</tr>
	<tr>
		<td>fecha</td> 
		<td>Date</td>
		<td>Fecha y hora en que se enrolo el usuario.</td>
	</tr>
	<tr>
		<td>ip</td> 
		<td>Cadena</td>
		<td>IP del usuario en donde se registró.</td>
	</tr>
</tbody>
</table>
<br>

**Código de respuesta fallida**
<table>
<thead>
	<tr>
		<th>Código</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>500</td> 
		<td>Fallo al registrar el usuario.</td>
	</tr>

</tbody>
</table>
<br>

**Parámetros de salida fallida:**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>error</td> 
		<td>Cadena</td> 
		<td>
Indica el tipo de error ocurrido.
</td> 
	</tr>
	<tr>
		<td>mensaje</td> 
		<td>Cadena</td> 
		<td>Muestra un detalle del error ocurrido.</td> 
	</tr>
</tbody>
</table>
<br>


**Ejemplo de parámetros de entrada:**
<br>
>{
	"cui": “1234567891234”,
	"email": “usuario@gmail.com”,
    "telefono": “12345678”,
	"foto_per": “http://imagen.jpg”,
    "foto_dpi": “http://imagen.jpg”,
	"pin": “12345”,
>}

**Ejemplo de parámetros de salida exitosa:**
<br>
>{
    “id”: “_id00001”
	"cui": “1234567891234”,
	"email": “usuario@gmail.com”,
    "telefono": “12345678”,
	"foto_per": “http://imagen.jpg”,
    "foto_dpi": “http://imagen.jpg”,
	"pin": “12345”,
	“fecha”: “DD/MM/YYYY”,
	“ip”: “10.0.0.0”
>}

**Ejemplo de parámetros de salida fallida:**
<br>
>{
	"status": 400,
	"mensaje" “”:    
>}