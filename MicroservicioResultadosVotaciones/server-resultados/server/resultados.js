
module.exports.Resultados= class Resultados{

    
    urlMicroservicioVotacion='https://mocki.io/v1/fe46441f-61ba-4319-b4d7-9dfc940c5719'
    
    constructor() {
    }
    
    isIdValido(id){
        if(id<=0){
            return false;
        }else{
            return true;
        }
    }

    isValidJsonResult(data){
        if(data!=undefined){
            if(data.status==200){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    jsonHasAnError(){
        if(data!=undefined){
            if(data.status!=200){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    async callOtherServices(id,callback){

        this.getTokenAxios(function(err,token){

            if(!err){
                callback(null,token.token);
                
            }else{
                callback(err,null);
            } 
        });
        

    }

    getTokenAxios(callback){
        var axios = require('axios');
        axios
        .post('http://34.70.175.232:89/api/token',{},
            {headers: {'Authorization': 'Basic Z3J1cG8wOTpnUjRwMF8wOQ=='}})
        .then(respuesta => {
            console.log(respuesta.data);
            
            return callback(null,respuesta.data);
        })
        .catch(error => {
            console.error(error);
            return callback(error,null);
        })
    }

    getToken(callback){
        http = require('http');

        var objAuth = {'Authorization': 'Basic Z3J1cG8wOTpnUjRwMF8wOQ=='};
        var optionsAuth = {
            host : '34.70.175.232',
            path : '/api/token',
            method : 'POST',
            port: 88,
            headers: objAuth
        };
        var bearerToken = "";
        console.log("->>>>>>  Consumiendo servicio de RENAP -> POST -> 34.70.175.232/api/token")
        var reqAuth = http.request(optionsAuth, function(res) {
            console.log("statusCode: ", res.statusCode);
            res.on('data', function(d) {
               
                var data = JSON.parse(d);
                bearerToken = data.jwt;
                
                
                return callback(bearerToken);
            });
        }.bind(this));
        reqAuth.end();
    }


    async llamarMicroservicioVotaciones(token,id,callback){
        console.log("Peticion a otro microservicio");
        var axios = require('axios');
        axios
        .get(`http://34.70.175.232/api/resultado/${id}`,
         {headers: {'Authorization': `Bearer ${token}`}})
        .then(respuesta => {
            console.log(`statusCode: ${respuesta.statusCode}`)
            console.log(respuesta.data);
            return callback(null,respuesta.data);
        })
        .catch(error => {
            console.error(error.data);
            return callback(error,null);
        })
    }

    sumar(num){
        return num+num;
    }
}

//module.exports=Resultados();