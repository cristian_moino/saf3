const express = require('express');
const app = express();
const mysql = require('mysql');
const morgan = require('morgan');
const bodyParser = require('body-parser');
var cors = require('cors');

const myConnection = require('express-myconnection');

var jwt=require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
//require('./database');

const Resultados=require('../server/resultados');

let microservicio=new Resultados.Resultados();

let respuesta = {
    error: false,
    codigo: 200,
    mensaje:''
   };


var publicKey = fs.readFileSync('server/public.key','utf8');

var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

// request log
app.use(morgan('dev'));
app.use(cors());
// connection to BD
/*
app.use(myConnection(mysql, {
    host: 'db-mysql',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'sa_bd'
  }, 'single'));
*/
app.use(express.urlencoded({extended: true}));
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Methods","GET,HEAD,POST,PUT,OPTIONS");
    res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,contentType,Content-Type,Accept,Authorization");
    next();
});
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.listen(82, ()=>{
    console.log('server on port 82');
})

/*
const pool = mysql.createPool({
    connectionLimit: 10,    
    host: 'db-mysql',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'sa_bd'
 
}); 
 */


votantesRegistrados = () =>{
    return new Promise((resolve, reject)=>{
        pool.query(`select count(*) 
            from Ciudadano c left join detalle_eleccion de 
            on c.idCiudadano=de.idCiudadano and de.idEleccion= ?  `, ideleccion,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
};


// Verificar token para autorizar o denegar el acceso
function verifytoken(req,res,next){
    const bearerHeader=req.headers['authorization'];

    if(typeof bearerHeader !=='undefined'){
        const bearerToken=bearerHeader.split(" ")[1];
        req.jwt=bearerToken;
        next();
    }else{
        res.sendStatus(403) 
    }
}

// Consulta el total de votantes registrados
function obtenerTotalVotantesRegistrados(ideleccion){
    req.getConnection((err, conn) => {
        conn.query(`select count(*) 
            from detalle_eleccion de inner join eleccion e
            on de.idEleccion=e.idEleccion and e.idEleccion=? `, ideleccion, (err, rows) => {
            res.json({resultado:rows});
        });
    });
}

//Consulta total de votantes en una eleccion
async function votantesQueVotaron(ideleccion){
    req.getConnection((err, conn) => {
        conn.query(`select count(*) 
            from Ciudadano c left join detalle_eleccion de 
            on c.idCiudadano=de.idCiudadano and de.idEleccion= ?  `, ideleccion, (err, rows) => {
            res.json({resultado:rows});
        });
    });
}

async function votosPorPartido(ideleccion){
    req.getConnection((err, conn) => {
        conn.query(`select p.nombrePartido 
            from detalle_eleccion de inner join partido p
            on de.idPartido=p.idPartido
            inner join candidato c 
            on p.idPartido=c.idPartido where de.idEleccion= ?  `, ideleccion, (err, rows) => {
            res.json({resultado:rows});
        });
    });
}

resultados={
    "titulo":"TituloEleccion",
    "fecha_inicio":"2021-03-01 00:00:00",
    "fecha_fin":"2021-03-02 20:00:00",
    "votantes_registrados":127,
    "votos_totales": 100,
    "datos_votacion":
    [
        {
        "opcion": "nulo",
        "valores":[],
        "votos_recibidos":10,
        "ubicacion":
            [
                {
                "latitud": "14.614551",
                "longitud": "-90.534457"
                },
                {
                "latitud": "14.630217",
                "longitud": "-90.600414"
                }
                                        //  …. Hasta el # de votos recibidos
            ]
        },
        {
            "opcion": "Planilla A UFM",
            "votos_recibidos": 15,
            "ubicacion":
                [
                    {
                    "latitud": "14.612203",
                    "longitud": "-90.518612"
                    },
                    {
                    "latitud": "14.623846",
                    "longitud": "-90.516534"
                    }
                                            //  …. Hasta el # de votos recibidos
                ],
                "valores":
                [
                    {
                        "key" : "Universidad",
                            "value" : "UFM"
                    },
                    {
                        "key" : "Decano",
                        "value" : "Juan Mata"
                    },
                    {
                        "key" : "Secretario",
                        "value" : "Luis Grisolia"
                    },
                    {
                        "key" : "Secretario",
                        "value" : "Rodrigo Cayarga"
                    }

                ]
            },
                {
                    "opcion": "Planilla B USAC",
                    "votos_recibidos": 44,
                    "ubicacion":
                                [
                        {
                        "latitud": "14.590039",
                        "longitud": "-90.503642"
                        },
                        {
                        "latitud": "3245324534",
                        "longitud": "345325342"
                        }
                                                //  …. Hasta el # de votos recibidos
                    ],
                    "valores":[
                        {
                                "key" : "Universidad",
                                "value" : "USAC"
                        },
                        {
                                "key" : "Decano",
                            "value" : "Guillermo Barreda"
                        },
                        {
                            "key" : "Secretario",
                            "value" : "Bruno Wencke"
                        },
                        {
                            "key" : "Secretario",
                            "value" : "Kenneth Pineda"
                        } 
                    ]
                }
    ]

};

resultados2={

	"status":200,
    "titulo":"Elecciones Presidente",
	"fecha_inicio":"09/04/2021",
	"fecha_fin":"10/04/2021",
	"votantes_registrados":127,
	"votos_totales":32,
	"cargo":"Presidente",
	"datos_votacion":{
		"nulos":{
			"postulante": "nulo",
			"votos_recibidos":42,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 40
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos":2
			}
			]
		},
		"PAN":{
			"postulante": { "presidente":"juan paco","vicepresidente":"pedro delamar" },
			"votos_recibidos":42,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 40
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos":2
			}
			]
		},
		"UNE":{
			"postulante": { "presidente" : "Pedro", "vicepresidente": "Manuel Carias"},
			"votos_recibidos": 80,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 40
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos":2
			},
			{
				"pais": "USA",
				"municipio": "",
				"departamento": "",
				"cantidad_votos":3
			}
			]
		}
	}
};



// Consultar resultados de votaciones
app.get('/api/:ideleccion', async function (req, res) {
    const  id  = req.params.ideleccion;


    //microservicio.getToken(function(token){res.json(token);     });

    microservicio.callOtherServices(id,function(err,token){
        if(!err){
            res.json({'token':token});
        }else{
            res.json({'error':err});
        } 
    });


    
    
});

// Consultar resultados de votaciones
app.get('/api/resultados/:ideleccion', verifytoken, async function (req, res) {

    const  id  = req.params.ideleccion;
    
    respuesta.error=true;
    console.log(req.jwt)
    jwt.verify(req.jwt,publicKey,verifyOptions,(err)=>{
        if(err){ 
            respuesta.codigo=403;
            respuesta.mensaje='No autorizado'
            res.send(respuesta);
        }else{
            if(microservicio.isIdValido(id)){
                //var decoded = jwt.decode(req.jwt, {complete: true});

                microservicio.callOtherServices(req.jwt,function(err,data){
                    if(err){
                        respuesta.codigo=404;
                        respuesta.mensaje='Bad Request'
                        res.send(respuesta);
                    }else{
                        microservicio.llamarMicroservicioVotaciones(data,id, async function(error,respuesta){
                            if(!error){
                                //callback(null,respuesta);
                                res.json(respuesta);
                            }else{
                                respuesta.codigo=404;
                                respuesta.mensaje='Bad Request'
                                res.send(respuesta);
                                //callback(error,null);
                            }
                        });
                        
                        /*
                        if(microservicio.isValidJsonResult(res)){
                            res.json({resultado:data});
                        }else{
                            respuesta.codigo=data.status;
                            res.json({respuesta});
                        }
                        */
            
                    }
                });                
                /*
                req.getConnection((err, conn) => {
                    conn.query('select titulo, fecha_inicio, fecha_fin from Eleccion where idEleccion=? ', id, (err, rows) => {
                        res.json({resultado:rows});
                    });
                });
                */
            }else{
                respuesta.error=true;
                respuesta.codigo=404;
                respuesta.mensaje='Bad Request'
                res.send(respuesta);
            }
        }
    });
});