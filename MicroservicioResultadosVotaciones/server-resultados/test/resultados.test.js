const assert = require('assert');
const Resultados=require('../server/resultados');

let microservicio=new Resultados.Resultados();


describe('Test Microservicio Resultados', () => {
    it('Is valid Json response from votacion microservice {status:200} should return true', () => {
            const respuesta={status:200,titulo:'Elecciones Presidente'};
            const res=microservicio.isValidJsonResult(respuesta);
            assert.ok(res);
       });
    it('Validate negative id -1 should return false', () => {
            const res=microservicio.isIdValido(-1);
            assert.equal(res,false);
       });
    it('Validate positive id 4 should return true', ()=>{
            const res=microservicio.isIdValido(4);
            assert.ok(res);
        });
    it('Validate positive id 10 should return true', ()=>{
        const res=microservicio.isIdValido(10);
        assert.ok(res);
    });
   });

   /*

   const expect = require('chai').expect;
   describe('Simple Math Test', () => {
    it('should return 2', () => {
           expect(1 + 1).to.equal(2);
       });
    it('should return 9', () => {
           expect(3 * 3).to.equal(9);
       });
   });

   */