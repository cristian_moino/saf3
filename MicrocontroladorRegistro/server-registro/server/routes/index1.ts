import { AxiosError, AxiosResponse} from "axios";
var express = require('express');
//var db = require('../db/index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
var fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})

function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');

//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/routes/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

router.post('/autenticacion', async (req, res, next) => {
    const username = '';
    const password = 'secret';
    const email = req.body.email;
    const pass = req.body.pass;    
    const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64')

    if(email == "grupo09" && pass == "gR4p0_09"){
        axios.post('http://34.70.175.232:88/api/token',{
        },{
            headers:{
                'Authorization': `Basic `+ token 
            }
        })
        .then(function(response:AxiosResponse){
            res.send(response.data);
            //res.send(email + " " + pass);
        }).catch(function(e:AxiosError){
            res.send(e.message);
        });
    }else{
        res.send("Error de usuario y Contraseña")
    }
});

function pedirToken(){
    const username = '';
    const password = 'secret';
    const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64');
        axios.post('http://34.70.175.232:88/api/token',{
        },{
            headers:{
                'Authorization': `Basic `+ token 
            }
        })
        .then(function(response:AxiosResponse){
            //res.send(email + " " + pass);
            return response.data;
        }).catch(function(e:AxiosError){
            return e.message;
        });
};

router.post('/login',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.login(req.body.email, req.body.password);
        jsonsalida = {"estado":201, "email": respuesta.email, "password": respuesta.password};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
/*
router.post('/login2',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.login2(req.body.email, req.body.password);
        jsonsalida = {"estado":201, "email": respuesta.email, "password": respuesta.password};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.post('/todos',   async (req, res, next) => {
    var valor = pedirToken();
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.all();
        jsonsalida = {"estado":201, "respuesta": respuesta, "token": valor};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.post('/login3',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.login3(req.body.email,req.body.password);
        jsonsalida = {"estado":201, "email": respuesta.email, "password": respuesta.password};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
*/
module.exports = router