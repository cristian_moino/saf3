"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var axios = require('axios');
var express = require('express');
var db = require('../db/index');
var router = express();
var mysql = require('mysql');
var morgan = require('morgan');
var myConnection = require('express-myconnection');
var bodyParser = require('body-parser');
var axios = require('axios');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var path = require('path');
var respuesta = {
    error: false,
    codigo: 200
};
//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/public.key', 'utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"
};
// middlewares
router.use(morgan('dev'));
router.use(myConnection(mysql, {
    host: 'db-mysql-registro',
    port: '3306',
    user: 'admin',
    password: 'root',
    database: 'prsa'
}, 'single'));
router.use(express.urlencoded({ extended: true }));
router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(bodyParser.json());
var logger = fs.createWriteStream('./server/log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
});
function addLog(newlog) {
    logger.write(newlog + "\n");
}
var http = require('http');
router.get('/todos', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var results, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.all()];
            case 1:
                results = _a.sent();
                res.json(results);
                return [3 /*break*/, 3];
            case 2:
                e_1 = _a.sent();
                console.log(e_1);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.post('/autenticacion', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var username, password, email, pass, token;
    return __generator(this, function (_a) {
        username = 'grupo09';
        password = 'gR4p0_09';
        email = req.body.email;
        pass = req.body.pass;
        token = Buffer.from(username + ":" + password, 'utf8').toString('base64');
        /*axios.post('http://35.232.54.106/token')
        .then(function(response:AxiosResponse){
            res.send(res.json(response.data.token));
        }).catch(function(e:AxiosError){
            res.send(e.message);
        });*/
        if (email == "grupo09" && pass == "gR4p0_09") {
            axios.post('http://34.70.175.232:88/api/token', {}, {
                headers: {
                    'Authorization': "Basic " + token
                }
            })
                .then(function (response) {
                res.send(response.data);
                //res.send(email + " " + pass);
            })["catch"](function (e) {
                res.send(e.message);
            });
        }
        else {
            res.send("Error de usuario y Contraseña");
        }
        return [2 /*return*/];
    });
}); });
router.post('/registarUser', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        functionLista(req, res, function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/];
                });
            });
        }.bind(this));
        return [2 /*return*/];
    });
}); });
function functionLista(req, res, callback) {
    listarUsuario(req, res, function () {
        return callback();
    }.bind(this));
}
function listarUsuario(req, res, callback) {
    console.log("Verificar auth...");
    llamarServicioAuth(function (bearerToken) {
        var _this = this;
        jwt.verify(bearerToken, publicKey, verifyOptions, function (err) {
            if (err) {
                console.log("error auth...");
                return callback();
            }
            else {
                //Logica para tirar dados......!
                console.log("llamando servicio listar...");
                llamarServicioListar(req, res, bearerToken, function () {
                    return callback();
                }.bind(_this));
            }
        });
    });
}
function llamarServicioAuth(callback) {
    var objAuth = { 'Authorization': 'Basic Z3J1cG8wOTpnUjRwMF8wOQ==' };
    var optionsAuth = {
        host: '34.70.175.232',
        path: '/api/token',
        method: 'POST',
        port: 89,
        headers: objAuth
    };
    var bearerToken = "";
    console.log("->>>>>>  Consumiendo servicio de RENAP -> POST -> 34.70.175.232/api/token");
    var reqAuth = http.request(optionsAuth, function (res) {
        console.log("statusCode: ", res.statusCode);
        res.on('data', function (d) {
            //console.info('POST result:'+d+'\n');
            //process.stdout.write(d+'\n');
            var data = JSON.parse(d);
            bearerToken = data.token;
            console.log("token: " + bearerToken);
            addLog("autenticación de token" + bearerToken + "\n");
            return callback(bearerToken);
        });
    }.bind(this));
    reqAuth.end();
}
function llamarServicioListar(req, res, bearerToken, callback) {
    var objAuth = { 'Authorization': 'Bearer ' + bearerToken };
    console.log(bearerToken);
    console.log(req.body.cui);
    var j = axios.get("http://34.70.175.232:87/api/renap/" + req.body.cui, {
        headers: {
            'Authorization': "Bearer " + bearerToken
        }
    })
        .then(function (response) {
        console.log("->>>>>>  Consumiendo servicio de listarUsuarios -> POST -> http://34.70.175.232:87/api/renap/:id");
        if (response.data.cui == req.body.cui && response.data.nombres == req.body.nombre && response.data.apellidos == req.body.apellido) {
            console.log("Usuario encontrado");
            console.log(response);
            addLog("Usuario encontrado en renap\n");
            var user = response.data;
            db.insertarUsuario(user.cui, user.nombres, user.apellidos, user.fecha_nacimiento, user.lugar_municipio, user.lugar_departamento, user.lugar_pais, user.nacionalidad, user.sexo, user.estado_civil, user.servicio_militar, user.privado_libertad, user.foto, user.padron, req.body.email, req.body.password, user.ip);
            res.send(response.data);
            return callback();
        }
        addLog("Usuario no encontrado en renap\n");
        res.send("Usuario no encontrado en renap\n");
        return callback("Usuario no existe");
    })["catch"](function (e) {
        console.log("Error bro :(");
        console.log(e);
        res.send(e.message);
    });
}
function pedirToken() {
    var username = '';
    var password = 'secret';
    var token = Buffer.from(username + ":" + password, 'utf8').toString('base64');
    axios.post('http://34.70.175.232:88/api/token', {}, {
        headers: {
            'Authorization': "Basic " + token
        }
    })
        .then(function (response) {
        //res.send(email + " " + pass);
        return response.data;
    })["catch"](function (e) {
        return e.message;
    });
}
;
//login
router.post('/login', function (req, res) {
    var user = req.body;
    console.log("esto trae el body" + req.body);
    req.getConnection(function (err, conn) {
        conn.query("SELECT * FROM Usuario WHERE email = ?", [user.email], function (err, rows) {
            if (err) {
                console.log(err);
                res.json(err);
            }
            if (user.password == rows[0].password) {
                addLog(" >> Login satisfactorio para usuario ");
                res.json({
                    mensaje: "Login satisfactorio.",
                    rows: rows
                });
            }
            else {
                res.json({
                    mensaje: "Login Error.",
                    code: 400
                });
            }
        });
    });
});
/*
router.post('/todos',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.all();
        jsonsalida = {"estado":201, "respuesta": respuesta};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.post('/login3',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.oneAll(req.body.cui);
        jsonsalida = {"estado":201, "respuesta": respuesta};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
*/
module.exports = router;
