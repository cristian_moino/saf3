const mysql = require('mysql');

const con = mysql.createConnection({
    host: "db-mysql",
    port: 3306,
    user: "root",
    password: "root",
    database: "sa_bd"
});
  
con.connect(function(err) {
    if (err) console.log("Error en conexion BD");
    console.log("BD Conectada.");
});
