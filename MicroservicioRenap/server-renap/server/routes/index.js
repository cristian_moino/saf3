"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var express = require('express');
var db = require('../db/index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
});
function addLog(newlog) {
    logger.write(newlog + "\n");
}
var http = require('http');
//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/public.key', 'utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"
};
router.get('/listarciudadanos', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var results, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.all()];
            case 1:
                results = _a.sent();
                res.json(results);
                return [3 /*break*/, 3];
            case 2:
                e_1 = _a.sent();
                console.log(e_1);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.get('/cargarcsv', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var results, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.cargarcsv()];
            case 1:
                results = _a.sent();
                res.json(results);
                return [3 /*break*/, 3];
            case 2:
                e_2 = _a.sent();
                console.log(e_2);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.get('/renap/:id', verifytoken, function (req, res, next) {
    console.log("jwt ", req.jwt);
    jwt.verify(req.jwt, publicKey, verifyOptions, function (err) { return __awaiter(void 0, void 0, void 0, function () {
        var decoded, results, e_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!err) return [3 /*break*/, 1];
                    addLog(" >> Error en token login.");
                    res.error = true;
                    res.codigo = 403;
                    res.send(res);
                    return [3 /*break*/, 5];
                case 1:
                    decoded = jwt.decode(req.jwt, { complete: true });
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, db.one(req.params.id)];
                case 3:
                    results = _a.sent();
                    //console.log("results", results);
                    res.json(results);
                    return [3 /*break*/, 5];
                case 4:
                    e_3 = _a.sent();
                    console.log(e_3);
                    res.sendStatus(500);
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    }); });
});
router.post('/registro', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var jsonsalida, idciudadano, e_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("insert ciudadano " + req.body.cui);
                jsonsalida = { 406: "error con comunicación a base de datos" };
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, db.insertarCiudadano(req.body.cui, req.body.nombres, req.body.apellidos, req.body.fecha_nacimiento, req.body.lugar_municipio, req.body.lugar_departamento, req.body.lugar_pais, req.body.nacionalidad, req.body.sexo, req.body.estado_civil, req.body.servicio_militar, req.body.privado_libertad, req.body.foto, req.body.padron)];
            case 2:
                idciudadano = _a.sent();
                jsonsalida = { "cui": req.body.cui, "nombres": req.body.nombres, "apellidos": req.body.apellidos, "fecha_nacimiento": req.body.fecha_nacimiento, "lugar_municipio": req.body.lugar_municipio, "lugar_departamento": req.body.lugar_departamento, "lugar_pais": req.body.lugar_pais, "nacionalidad": req.body.nacionalidad, "sexo": req.body.sexo, "estado_civil": req.body.estado_civil, "servicio_militar": req.body.servicio_militar, "privado_libertad": req.body.privado_libertad, "foto": req.body.foto, "padron": req.body.padon };
                res.json(jsonsalida);
                addLog("ciudadano insertardo:" + req.body.cui + "\n");
                return [3 /*break*/, 4];
            case 3:
                e_4 = _a.sent();
                console.log(e_4);
                res.sendStatus(500);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
function verifytoken(req, res, next) {
    var bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        var bearerToken = bearerHeader.split(" ")[1];
        req.jwt = bearerToken;
        next();
    }
    else {
        res.sendStatus(403); //ruta o acceso prohibido
    }
}
module.exports = router;
