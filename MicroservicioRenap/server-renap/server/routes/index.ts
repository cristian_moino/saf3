import { AxiosError, AxiosResponse} from "axios";
import { json } from "express";
var express = require('express');
var db = require('../db/index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
const path = require('path');
var fs = require('fs');
const jwt = require('jsonwebtoken');



var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');



//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

router.get('/listarciudadanos', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.get('/cargarcsv', async (req, res, next) => {
    try{
        let results = await db.cargarcsv();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/renap/:id', verifytoken, (req, res, next) => {
    console.log("jwt ", req.jwt);
    jwt.verify(req.jwt,publicKey,verifyOptions, async (err)=>{
        if(err){
            addLog(" >> Error en token login.");   
            res.error=true
            res.codigo=403
            res.send(res);
        }else{

            var decoded =  jwt.decode(req.jwt, {complete: true});
            try{
                //console.log("entro al try");
                let results = await db.one(req.params.id);
                //console.log("results", results);
                res.json(results);
                
            } catch(e) {
                console.log(e);
                res.sendStatus(500);
            }
        }
    });
});
router.post('/registro',   async (req, res, next) => {

    console.log("insert ciudadano "+req.body.cui);
    var jsonsalida : any = {406: "error con comunicación a base de datos"};
    try{
        let idciudadano = await db.insertarCiudadano(req.body.cui,req.body.nombres,req.body.apellidos,req.body.fecha_nacimiento,req.body.lugar_municipio,req.body.lugar_departamento,req.body.lugar_pais, req.body.nacionalidad,req.body.sexo,req.body.estado_civil,req.body.servicio_militar,req.body.privado_libertad,req.body.foto,req.body.padron);
        
        jsonsalida = {"cui":req.body.cui,"nombres":req.body.nombres,"apellidos":req.body.apellidos,"fecha_nacimiento":req.body.fecha_nacimiento,"lugar_municipio":req.body.lugar_municipio,"lugar_departamento":req.body.lugar_departamento,"lugar_pais":req.body.lugar_pais,"nacionalidad":req.body.nacionalidad,"sexo":req.body.sexo,"estado_civil":req.body.estado_civil,"servicio_militar":req.body.servicio_militar,"privado_libertad":req.body.privado_libertad,"foto":req.body.foto,"padron":req.body.padon};
        res.json(jsonsalida);
        addLog("ciudadano insertardo:"+req.body.cui+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }


    
});



function verifytoken(req,res,next){
    const bearerHeader=req.headers['authorization'];

    if(typeof bearerHeader !=='undefined'){
        const bearerToken=bearerHeader.split(" ")[1];

        req.jwt=bearerToken;

        next();
    }else{
        res.sendStatus(403) //ruta o acceso prohibido
    }
}
module.exports = router