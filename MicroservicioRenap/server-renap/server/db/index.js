var axios = require('axios');
const mysql = require('mysql');

var fs = require('fs');
const fastcsv = require("fast-csv");
let stream = fs.createReadStream("./server/cargar.csv");
let csvData = [];
let csvStream = fastcsv
var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

const con = mysql.createConnection({

    host: 'db-mysql-renap',
    port: '3306',
    user: 'admin',
    password: 'root',

    database: 'prsa'
    /*
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1234',

    database: 'prsa'
    */
});
let proyectobd = {};

proyectobd.all = () => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM Renap;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });


};

proyectobd.cargarcsv = () => {
    return new Promise((resolve, reject) => {
        let csvStream = fastcsv
            .parse()
            .on("data", function(data) {
            csvData.push(data);
            })
            .on("end", function() {
            // remove the first line: header
            csvData.shift();

            // create a new connection to the database
            /*const connection = mysql.createConnection({
                host: 'localhost',
                port: '3306',
                user: 'root',
                password: '1234',

                database: 'prsa'
            });
            */
            const connection = mysql.createConnection({
                host: 'db-mysql-renap',
                port: '3306',
                user: 'root',
                password: 'root',
                database: 'prsa'
            });

            // open the connection
            connection.connect(error => {
                if (error) {
                console.error(error);
                } else {
                let query =
                    "INSERT INTO Renap(cui, nombres, apellidos, fecha_nacimiento, lugar_municipio, lugar_departamento, lugar_pais, nacionalidad, sexo, estado_civil, servicio_militar, privado_libertad, foto, padron) VALUES ?";
                connection.query(query, [csvData], (error, response) => {
                    console.log(error || response);
                });
                }
            });
            });
            
            console.log("csvData ", csvData)
            //resolve({"insertados":"correcto"});
            resolve(stream.pipe(csvStream));
        });

};
proyectobd.one = (idciudadano) => {
    
        
    return new Promise((resolve, reject) => {
        console.log("entro al one",idciudadano);
        const query = "SELECT * FROM Renap WHERE Cui =" + idciudadano + ";";
        con.query(query, (err, res) => {
        if (err) throw err;
        console.log("res: ", res[0]);
        return resolve(res[0]);
        });
    });


};
proyectobd.insertarCiudadano = (cui,nombres,apellidos,fecha_nacimiento, lugar_municipio, lugar_departamento,lugar_pais, nacionalidad, sexo, estado_civil, servicio_militar, privado_libertad, foto,padron) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO Renap(Cui,Nombres,Apellidos,Fecha_nacimiento, Lugar_municipio, Lugar_departamento, Lugar_pais, Nacionalidad, Sexo, Estado_civil, Servicio_militar, Privado_libertad, Padron, Foto) VALUES ('" + cui + "','" + nombres + "','" + apellidos + "','"+fecha_nacimiento +"','" + lugar_municipio + "','"+ lugar_departamento +"','"+ lugar_pais+"','"+ nacionalidad + "','" +sexo + "'," +estado_civil + ","+servicio_militar +"," +privado_libertad + ",'"+ foto +"',"+ padron+");";
        con.query(query, (err, res) => {
        if (err) throw err;           

        addLog("registro ciudadano: "+res.insertId+"\n");
        
        resolve(res.insertId);
        });
    });


};

module.exports = proyectobd;
