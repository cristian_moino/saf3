import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConectionService } from '../services/conection.service';
import { ApiService } from '../services/api.service';
import { IpServiceService } from '../ip-service.service';
import { RutasService } from '../services/rutas.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  correo: string = '';
  password: string = '';
  alerta: string = '';
  userid: any;
  ipAddress:string | undefined; 
  
  getIP()  
  {  
    this.ip.getIPAddress().subscribe((res:any)=>{  
      this.ipAddress=res.ip;  
    });  
  }  

  constructor(private ip:IpServiceService, private con: ConectionService, private rutas:RutasService, private router: Router) { }
  
  ngOnInit(): void {
    sessionStorage.clear();
    this.userid = sessionStorage.getItem('userid');
    console.log(this.userid)
    if (!this.userid || this.userid  == null){
      this.router.navigate(['login']);
    }
    this.getIP();
  }

  createLoginObject(email: string, password: string) {
    return { email: email, password: password};
  }

  createLoginGObject(email: string, pass: string) {
    return { email: email, pass: pass};
  }

  login() {
    console.log("entro");
    let login:any;
    if(this.correo=="grupo01" || this.correo == "grupo02" || this.correo == "grupo04"
    || this.correo == "grupo05" || this.correo == "grupo07" || this.correo == "grupo08"){
      console.log("que onda");
      login = this.createLoginGObject(this.correo, this.password);
      let rep= this.rutas.devolverAutenticacion(this.correo,this.password);
      this.con.PostRequest3(rep.url + rep.api, login).toPromise()
      .then((res) => {
        console.log(res);
        if(res.token){
          sessionStorage.setItem('usuario', this.correo);
          sessionStorage.setItem('contra', this.password);
          sessionStorage.setItem('token', res.token);
          this.router.navigate(['inicio-Admin']);
        }
        else{
          this.alerta = res.mensaje;
        }
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
    }
    else if(this.correo == "grupo09"){
      console.log("que onda");
      login = this.createLoginGObject(this.correo, this.password);
      let rep= this.rutas.devolverAutenticacion(this.correo,this.password);
      console.log(rep);
      this.con.PostRequest3(rep.url + rep.api, login).toPromise()
      .then((res) => {
        console.log(res);
        if(res.token || res.jwt){
          sessionStorage.setItem('usuario', this.correo);
          sessionStorage.setItem('contra', this.password);
          if(res.token){
            sessionStorage.setItem('token', res.token);
          }
          else if(res.jwt){
            sessionStorage.setItem('token', res.jwt);
          }
          this.router.navigate(['inicio-Admin']);
        }
        else{
          this.alerta = res.mensaje;
        }
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
    }
    else{
      login = this.createLoginObject(this.correo, this.password);
      this.con.PostRequest2('api/login', login).toPromise()
      .then((res) => {
        console.log(res);
        console.log("ya funciono");
        let resul = res.rows;
        if(res.rows){
          console.log(resul[0]);
          this.router.navigate(['inicioVotante']);
        }else{
          this.alerta = "Error al momento de autenticación F"
          console.log(this.alerta);
        }
        //let usuario = resul['rows'];  
        //console.log(usuario);
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
    }
  }

}
