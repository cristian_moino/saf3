import { Component, OnInit } from '@angular/core';

import { ResultadosService } from '../services/resultados.service';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label, SingleDataSet } from 'ng2-charts';
import { MapType } from '@angular/compiler';

import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';
import { RutasService } from '../services/rutas.service';

@Component({
  selector: 'app-res-votacion',
  templateUrl: './res-votacion.component.html',
  styleUrls: ['./res-votacion.component.css']
})
export class ResVotacionComponent implements OnInit {

  constructor(private resultadosService:ResultadosService,private rutasService:RutasService) { }

  resultados:any=[];
  ganador:any={votos_recibidos:0};
  votantesRegistrados:Number=0;
  numeroVotantes:Number=0;
  porcentajeGanador='';
  idresultados=0;

  public chartBarras: any = null; //Grafica de barras incluyendo nulos
  public chartPie: any = null; //Grafica de Pie incluyendo nulos
  private intervalUpdate: any = null;


  /*
    Grafica de Barras
  */
  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Cantidad de Votos' },
  ];

  lineChartLabels: Label[]=[];


  lineChartOptions = {
    responsive: true,
    scaleBeginAtZero : true,
    
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero: true
          }
      }]
  }
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: '#bd3005',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'bar' as const;


  /*
    Grafica de Pie
  */
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)','rgba(228, 63, 17, 1)',
                  'rgba(17, 136, 228, 1)','rgba(105, 17, 228, 1)'],
    },
  ];


  /*
    Mapa
  */
  mapTypeId:any="hybrid";
  zoom=10;
  lat=14.614551;
  long=-90.534457;
  listaPosiciones:any=[];

  listaElecciones:any=[];
  urlResultados="http://34.70.175.232:82/api/resultado/";
  urlElecciones="http://34.70.175.232/api/eleccion";

  ngOnInit(): void {
    this.cargarResultados(this.idresultados);
    this.startTimer();
    this.cargarListadoElecciones();
    /*
    navigator.geolocation.getCurrentPosition(position=>{
      this.lat=position.coords.latitude;
      this.long=position.coords.longitude;
      this.zoom=10;
    });
    */
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalUpdate);
  }

  startTimer() {
    this.intervalUpdate = setInterval(() => {
      this.cargarResultados(this.idresultados);
    },2000)
  }

  focusOutUrl(){
    console.log("Actualizando url ",this.urlElecciones);
    this.cargarListadoElecciones();
  }

  cargarListadoElecciones(){
    let usuario=sessionStorage.getItem('usuario');
    let contra=sessionStorage.getItem('contra');
    
    let path=this.rutasService.devolverEleccion(usuario,contra);
    this.urlElecciones=path.url+path.api;

    this.resultadosService.getElecciones(this.urlElecciones).subscribe(
      (res:any)=>{
        this.listaElecciones=res;
      }
    );
  }

  cargarResultados(ideleccion:Number){
    let usuario=sessionStorage.getItem('usuario');
    let contra=sessionStorage.getItem('contra');
    
    let path=this.rutasService.devolverResultados(usuario,contra);
    this.urlResultados=path.url+path.api;
    this.resultadosService.getResultados(ideleccion,this.urlResultados).subscribe(
      (res:any)=>{
        this.resultados=res;
        
        this.cargarGrafica();
        if(this.resultados.datos_votacion!=undefined){
          this.listaPosiciones=this.resultados.datos_votacion.ubicacion;
        }
      }
    );
  }


  cargarGrafica(){
    this.numeroVotantes=0;
    this.porcentajeGanador='';
    let data:number[]=[];
    this.lineChartLabels=[];
    this.pieChartLabels=[];

    for (let i = 0; i < this.resultados.datos_votacion.length; i++) {
      
      let item=this.resultados.datos_votacion[i];

      this.numeroVotantes+=item.votos_recibidos;

      if(item.votos_recibidos>this.ganador.votos_recibidos){
        this.ganador=item;
      }

      this.lineChartLabels.push(item.opcion);
      this.pieChartLabels.push(item.opcion);

      data.push(Number(item.votos_recibidos));
      
    }
    /*
    for (const [key, value] of Object.entries(this.resultados.datos_votacion)) {
      
      let valor:any=value;
      this.numeroVotantes+=valor.votos_recibidos;

      if(valor.votos_recibidos>this.ganador.datos.votos_recibidos){
        this.ganador={nombre:key,datos:value};
      }

      this.lineChartLabels.push(key);
      this.pieChartLabels.push(key);

      data.push(Number(valor.votos_recibidos));

    }
    */
    this.porcentajeGanador=(Number(this.ganador.votos_recibidos)*100/Number(this.numeroVotantes)).toFixed(2);
    this.lineChartData[0].data=data;
    this.pieChartData=data;

  }




  onChangeId(valor:any){
    this.idresultados=valor;
    console.log('Cambio ',valor);
  }



}
/*
  userid: string = "";
  resultado: any = [];
  elecciones: any = [];
  alerta: string = '';
  color:string = "#65ff00";
  conex: any;
    
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    //this.userid = sessionStorage.getItem('userid');
    if (!this.userid){
      this.router.navigate(['login']);
    }
    this.mostrar();
  }
  
  mostrar(): void {
    this.con.PostRequest('getComentario',this.conex).toPromise()
    .then((res)=>{
        for(let v in res){
          this.resultado = res[v];
        }
    }).catch((err) => {
      setTimeout(()=> this.alerta = err.console.error.mensaje,0);
    });

    

  }
*/


/*
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };

  public pieChartLabels: Label[] = [['SciFi'], ['Drama'], 'Comedy'];
  public pieChartData: SingleDataSet = [30, 50, 20];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
*/


