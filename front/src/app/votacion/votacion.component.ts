import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import { IpServiceService } from '../ip-service.service';
import { DatePipe } from '@angular/common'
import { withLatestFrom } from 'rxjs/internal/operators';
import { RutasService } from '../services/rutas.service';
@Component({
  selector: 'app-votacion',
  templateUrl: './votacion.component.html',
  styleUrls: ['./votacion.component.css']
})


export class VotacionComponent implements OnInit {
  todayNumber: number = Date.now();
  todayDate : Date = new Date();
  date: any;
  eleccion: string = '';
  partido: string = '';
  pin: string = '';
  mensaje: string = '';
  user: any;
  pass: any;
  token: any;
  fecha : any;
  hora:any;
  cui:any;
  elec:any;
  opcion:any;
  toppings = new FormControl();
  contact = {
    firstName: "CFR",
    comment: "No comment",
    subscribe: true,
    contactMethod: 2 // this id you'll send and get from backend
  }
  
  Elecciones = [
    { id: 1, label: "Email" },
    { id: 2, label: "Phone" }
  ]

  Partidos = [
    { id: 1, label: "Email" },
    { id: 2, label: "Phone" }
  ]

  pinc(): void{

  }

  constructor(public datepipe: DatePipe, private ip:IpServiceService, private con: ConectionService, private rutas: RutasService, private router: Router) { }
  
  votar(): void{

  }
    
  ngOnInit(): void {
    this.user = sessionStorage.getItem('usuario');
    this.pass = sessionStorage.getItem('contra');
    this.token = sessionStorage.getItem('token');
    console.log(this.token);
    
    this.llenarEleccion();
    this.llenarPartido();
    console.log()
  }

  
  createLoginGObject(email: string, pass: string) {
    return { email: email, pass: pass};
  }



  createPartidoObject(eleccion: string) {
    return { eleccion: eleccion};
  }

  createEleccionObject(fecha: string, hora: string) {
    return { fecha: fecha, hora: hora };
  }

  createVotacionObject(cui:string, ideleccion:number, idopcion: number){
    return { cui: cui, ideleccion: ideleccion, idopcion:idopcion };
  }
  
  horas(){
    this.date=new Date();
    let latest_date =this.datepipe.transform(this.date, 'h:mm:ss');
    return latest_date;
  }

  fechas(){
    this.date=new Date();
    let latest_date =this.datepipe.transform(this.date, 'yyyy-MM-dd');
    return latest_date;
   }

  llenarEleccion(){
    this.fecha = this.fechas()?.toString();
    this.hora = this.horas()?.toString();
    console.log(this.horas());
    console.log(this.fechas());
    let votacion = this.createVotacionObject(this.cui, this.elec, this.opcion);

    this.con.PostRequest4('http://34.70.175.232/api/votacion', votacion, this.token).toPromise()
      .then((res) => {
        this.mensaje = res;
        console.log(res);
        //this.router.navigate(['inicioVotante']);
      })
      .catch((err)=>{
        setTimeout(() => this.mensaje = err.error.mensaje, 0);
      });
      /*
    this.con.PostRequest2('login', eleccion).toPromise()
      .then((res) => {
        console.log("ya funciono");  
        console.log(res.res);
        res.res = 
        { id: 1, label: "Email" },
        { id: 2, label: "Phone" }
        ;
        this.Elecciones = [res.res];
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
      */
  }

  llenarPartido(){
    let eleccions = this.createPartidoObject(this.eleccion);
    this.con.PostRequest2('login', eleccions).toPromise()
      .then((res) => {
        console.log("ya funciono");  
        console.log(res.res);
        res.res = 
        { id: 1, label: "Email" },
        { id: 2, label: "Phone" }
        ;
        this.Partidos = [res.res];
      })
      .catch((err)=>{
        setTimeout(() => this.mensaje = err.error.mensaje, 0);
      });
  }

  mostrar(): void {
    
  }

  

}

