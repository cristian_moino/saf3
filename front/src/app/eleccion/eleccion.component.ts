import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';
import { IpServiceService } from '../ip-service.service';
import { stringify } from '@angular/compiler/src/util';
import { RutasService } from '../services/rutas.service';

@Component({
  selector: 'app-eleccion',
  templateUrl: './eleccion.component.html',
  styleUrls: ['./eleccion.component.css']
})


export class EleccionComponent implements OnInit {

  user: any = "";
  pass: any = "";
  token: any = "";
  titulo: any;
  descripcion: any;
  fecha_inicio: any;
  hora_inicio: any;
  fecha_fin: any;
  hora_fin: any;
  opciones: any;
  mensaje: string = "";

  /*
  {
    "titulo": "Elecciones presidenciales",
    "descripcion": "Elecciones para presidente",
      "fecha_inicio": "DD/MM/YYYY",
      "hora_inicio" : "13:00",
    "fecha_fin": "DD/MM/YYYY",
      "hora_fin" : "16:00",
      "opciones": [ 1 , 2 ,5, 6]            
  }
  */

  //http://34.70.175.232:81/api/listaelecciones

  constructor(private ip:IpServiceService, private con: ConectionService, private rutas:RutasService, private router: Router) { }
  
  createEleccionObject(titulo: string, descripcion: string, fecha_inicio: string, hora_inicio: string, fecha_fin: string, hora_fin: string, opciones: any) {
    return { titulo, descripcion, fecha_inicio, hora_inicio, fecha_fin, hora_fin, opciones };
  }

  ngOnInit(): void {
    this.user = sessionStorage.getItem('usuario');
    this.pass = sessionStorage.getItem('contra');
    this.token = sessionStorage.getItem('token');
    /*if (!this.userid){
      this.router.navigate(['login']);
    }*/
  }
  
  eleccion(): void {
    let elec = this.createEleccionObject(this.titulo, this.descripcion, this.fecha_inicio, this.hora_inicio, this.fecha_fin, this.hora_fin, [1,2,3]);
    console.log(this.user);
    console.log(this.pass);
    console.log(this.token);
    console.log(elec);
    //ellos van a consumir eleccion
    this.con.PostRequest4('http://34.70.175.232/api/elecciones', elec, this.token).toPromise()
      .then((res) => {
        this.mensaje = res;
        console.log(res);
        //this.router.navigate(['inicioVotante']);
      })
      .catch((err)=>{
        setTimeout(() => this.mensaje = err.error.mensaje, 0);
      });
  }
}
