import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RutasService {

  constructor() { }

  devolverAutenticacion(usuario:any, contra:any){
    if(usuario == "grupo01" && contra == "gR4p0_01"){
      return {url:'http://micro1.tk:3001/',api: 'api/autenticacion'};
    }
    else if(usuario == "grupo02" && contra == "gR4p0_02"){
      return {url:'http://35.188.141.106:8092/',api: 'api/autenticacion'};
    }
    else if(usuario == "grupo04" && contra == "gR4p0_04"){
      return {url:'http://35.222.62.134:3005/',api: 'api/autenticacion'};
    }
    else if(usuario == "grupo05" && contra == "gR4p0_05"){
      return {url:'http://34.70.175.232:81/',api: 'api/autenticacion'};
    }
    else if(usuario == "grupo06" && contra == "gR4p0_07"){
      return {url:'http://3.141.201.143:8000/',api: 'api/autenticacion'};
    }
    else if(usuario == "grupo08" && contra == "gR4p0_08"){
      return {url:'http://34.73.67.52/',api: 'api/autenticacion'};
    }
    else if(usuario == "grupo09" && contra == "gR4p0_09"){
      return {url:'http://34.70.175.232:89/',api: 'api/autenticacion'};
    } else {
      return {url:'http://34.70.175.232:81/',api: 'api/autenticacion'};
    }
  }

  devolverRenap(usuario:any, contra:any){
    if(usuario == "grupo01" && contra == "gR4p0_01"){
      return {url: 'http://micro2.tk:3002/',api: 'api/renap/'};
    }
    else if(usuario == "grupo02" && contra == "gR4p0_02"){
      return {url:'http://35.188.141.106:8091/',api: 'api/renap/'};
    }
    else if(usuario == "grupo04" && contra == "gR4p0_04"){
      return {url:'http://35.222.62.134:3000/',api: 'api/renap/'};
    }
    else if(usuario == "grupo05" && contra == "gR4p0_05"){
      return {url:'http://34.70.175.232:3000/',api: 'api/renap/'};
    }
    else if(usuario == "grupo06" && contra == "gR4p0_07"){
      return {url:'http://3.141.201.143:8001/',api: 'api/renap/'};
    }
    else if(usuario == "grupo08" && contra == "gR4p0_08"){
      return {url:'http://34.73.67.52/',api: 'api/renap/'};
    }
    else if(usuario == "grupo09" && contra == "gR4p0_09"){
      return {url:'http://34.70.175.232:87/',api: 'api/renap/'};
    } 
    else {
      return {url:'', api:''};
    }
  }

  devolverResultados(usuario:any, contra:any){
    if(usuario == "grupo01" && contra == "gR4p0_01"){
      return {url:'',api: 'api/resultado/'};
    }
    else if(usuario == "grupo02" && contra == "gR4p0_02"){
      return {url:'http://35.188.141.106:8090/',api: 'api/resultado/'};
    }
    else if(usuario == "grupo04" && contra == "gR4p0_04"){
      return {url:'http://35.222.62.134:3006/',api: 'api/resultado/'};
    }
    else if(usuario == "grupo05" && contra == "gR4p0_05"){
      return {url:'http://34.70.175.232:3004/',api: 'api/resultado/'};
    }
    else if(usuario == "grupo06" && contra == "gR4p0_07"){
      return {url:'http://3.141.201.143:8002/',api: 'api/resultado/'};
    }
    else if(usuario == "grupo08" && contra == "gR4p0_08"){
      return {url:'http://34.73.67.52/',api: 'api/resultado/'};
    }
    else if(usuario == "grupo09" && contra == "gR4p0_09"){
      return {url:'http://34.70.175.232:82/',api: 'api/resultado/'};
    } 
    else {
      return {url:'', api:''};
    }
  }

  devolverEleccion(usuario:any, contra:any){
    if(usuario == "grupo01" && contra == "gR4p0_01"){
      return {url:'http://micro1.tk:3005/',api: 'api/eleccion'};
    }
    else if(usuario == "grupo02" && contra == "gR4p0_02"){
      return {url:'http://35.188.141.106:8094/',api: 'api/eleccion'};
    }
    else if(usuario == "grupo04" && contra == "gR4p0_04"){
      return {url:'http://35.222.62.134:3003/',api: 'api/eleccion'};
    }
    else if(usuario == "grupo05" && contra == "gR4p0_05"){
      return {url:'http://34.70.175.232:81/',api: 'api/eleccion'};
    }
    else if(usuario == "grupo06" && contra == "gR4p0_07"){
      return {url:'http://3.141.201.143:5004/',api: 'api/eleccion'};
    }
    else if(usuario == "grupo08" && contra == "gR4p0_08"){
      return {url:'http://34.73.67.52/',api: 'api/eleccion'};
    }else if(usuario == "grupo09" && contra == "gR4p0_09"){
      return {url:'http://34.70.175.232/',api: 'api/eleccion'};
    } else {
      return {url:'', api:''};
    }
  }

}
