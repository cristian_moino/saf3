import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

const httpAddress = 'http://34.70.175.232:88/';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  PostRequest(serverAddress: string, info: object) {
    var httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Basic ' + btoa('grupo09:gR4p0_09')
      })
    };
    return this.http.post<any>(httpAddress + serverAddress, info, httpOptions);
  }
}
