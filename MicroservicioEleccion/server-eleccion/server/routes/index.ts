import { AxiosError, AxiosResponse} from "axios";
import { json } from "express";
var express = require('express');
var db = require('../db/index');
let database = require('../db/indexblockchain');

var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
const path = require('path');
var fs = require('fs');
const jwt = require('jsonwebtoken');
const BlockChain = require("../blockchain/blockChain");
let hash = require('object-hash');
var logger = fs.createWriteStream('./server/log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');
var opcion;
var opciones=[];




//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "3600s"    
};

router.get('/listaelecciones', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
router.get('/eleccion', verifytoken,async (req, res, next) => {
    jwt.verify(req.jwt,publicKey,verifyOptions, async (err)=>{
        if(err){
            addLog(" >> Error en token login.");   
            res.error=true
            res.codigo=403
            res.send(res);
        }else{
            try{
                let results = await db.all();
                res.json(results);
                
            } catch(e) {
                console.log(e);
                res.sendStatus(500);
            }
        }
    });
});


router.get('/geteleccion/:id', verifytoken, (req, res, next) => {
    jwt.verify(req.jwt,publicKey,verifyOptions, async (err)=>{
        if(err){
            addLog(" >> Error en token login.");   
            res.error=true
            res.codigo=403
            res.send(res);
        }else{
            try{
                let results = await db.one(req.params.id);
                let ops = [];
                var count = 0;
                results.forEach( async( element) => {
                    //console.log("id opcion ",element.id_opcion );
                    try{
                        opcion =  await db.getCandidato(element.id_opcion);
                        ops.push(opcion);
                        count++;
                    }catch(e){
                        console.log(e);
                        res.sendStatus(500);    
                    }
                    if (count == results.length) res.json(ops)
                });
            } catch(e) {
                console.log(e);
                res.sendStatus(500);
            }
        }
    });
});
router.post('/elecciones',   async (req, res, next) => {

    console.log("insert eleccion "+req.body.titulo);
    let op = req.body.opciones
    //id opcion nulo
    op.push(7);
    console.log(op);
    var jsonsalida : any = {406: "error con comunicación a base de datos"};
    try{
        let ideleccion = await db.insertarEleccion(req.body.titulo,req.body.descripcion,req.body.fecha_inicio,req.body.hora_inicio,req.body.fecha_fin,req.body.hora_fin, op);
        
        req.body.opciones.forEach(element => {
            console.log("insertar detalle eleccion ",ideleccion + " "+element);
            db.insertarDetalleEleccion(ideleccion,element);
        });
        jsonsalida = {"estado":201,"id":ideleccion,"titulo":req.body.titulo,"descripcion":req.body.descripcion,"fecha_inicio":req.body.fecha_inicio,"hora_inicio":req.body.fecha_inicio,"fecha_fin":req.body.fecha_fin,"hora_fin":req.body.hora_inicio,"opciones":op};
        res.json(jsonsalida);
        addLog("eleccion insertardo:"+req.body.titulo+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }


    
});



router.post('/votacion',   verifytoken, (req, res, next) => {
    console.log("jwt ", req.jwt);
    jwt.verify(req.jwt,publicKey,verifyOptions, async (err)=>{
        if(err){
            addLog(" >> Error en token login.");   
            res.error=true
            res.codigo=403
            res.send(res);
        }else{
            
            
            var jsonsalida : any = {406: "error con comunicación a base de datos"};
            try{
                let ideleccion = await db.oneEleccion(req.body.ideleccion);
                var today = new Date();
                //console.log("id eleccion", ideleccion, "fecha inicio ",ideleccion[0].fecha_inicio, "fecha fin",ideleccion[0].fecha_fin, "fecha actual",(today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear());
                var dateFrom = ideleccion[0].fecha_inicio;
                var dateTo = ideleccion[0].fecha_fin;
                var timeFrom = ideleccion[0].hora_inicio;
                var timeTo = ideleccion[0].hora_fin;
                console.log("hora i",timeFrom,"hora f ", timeTo);
                var date = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear();

                var dateCheck = date;

                var d1 = dateFrom.split("/");
                var t1 = timeFrom.split(":");
                var t = parseInt(t1[0]) * 60 + parseInt(t1[1]);
                console.log(t1);
                console.log(t);
                var d2  = dateTo.split("/");
                var t2 = timeTo.split(":");
                var t3 = parseInt(t2[0]) * 60 + parseInt(t2[1]);
                console.log(t2);
                console.log(t3);
                var c = dateCheck.split("/");
                var ch = today.getHours() * 60 + today.getMinutes();
                console.log(ch);
                
                var from = new Date( d1[2],d1[0]-1, d1[1]);  
                var to   = new Date( d2[2],d2[0]-1, d2[1]);
                var check = new Date( parseInt(c[2]),parseInt(c[0])-1, parseInt(c[1]));
                console.log("from ", from, "to ", to, "check ", check);
                console.log(check > from && check < to)
                if((check > from && check < to) == false  ){
                    jsonsalida = {"estado":404,"error": "eleccion no disponible"};
                    res.json(jsonsalida);    
                    
                }else{
                    let idvoto =  await db.insertarVoto(req.body.cui,req.body.ideleccion,req.body.idopcion);
                    console.log("id voto ", idvoto);
                    var blockChain = new BlockChain();
                    blockChain.addNewTransaction(req.body.cui,req.body.ideleccion,req.body.idopcion);
                    blockChain.addNewBlock(null);
                    //console.log("Chain: ",blockChain.chain)
                    jsonsalida = {"estado":201,"id":idvoto,"cui":req.body.Cui,"ideleccion":req.body.ideleccion,"idopcion":req.body.idopcion};
                    res.json(jsonsalida);
                    addLog("voto insertardo:"+idvoto+"\n");
                }
                
                
            } catch(e) {
                console.log(e);
                res.sendStatus(400);
            }
        }
    });
});
router.get('/resultado/:id',   verifytoken, (req, res, next) => {
    console.log("jwt ", req.jwt);
    jwt.verify(req.jwt,publicKey,verifyOptions, async (err)=>{
        if(err){
            addLog(" >> Error en token .");   
            res.error=true
            res.codigo=403
            res.send(res);
        }else{
            var jsonsalida : any = {406: "error con comunicación a base de datos"};
            try{
                //console.log("id eleccion", req.params.id);
                listaUsers(res,async function(){
                }.bind(this));
                let results = await db.allVotacionbyId(req.params.id);
                addLog("get resultados votacion de la eleccion:"+req.params.id+"\n");
                jsonsalida = {"Usuarios registrados":a ,"votos":results}
                res.json(jsonsalida);
            } catch(e) {
                console.log(e);
                res.sendStatus(400);
            }
        }
    });
});
function listaUsers(res,callback){
    axios.get('http://34.70.175.232:81/api/todos')
    .then(function(response:AxiosResponse){
        console.log("funcion lista users");
        //console.log("response: "+response.data);
        
        var count = 0;
        response.data.forEach( async( element) => {
            console.log("id opcion ",element.cui );
            try{
                console.log("entro al try ", count);
                ops.push(element.cui);
                count++;
            }catch(e){
                console.log(e);
                //res.sendStatus(500);    
            }
            console.log("ops ",ops);
            if (count == response.data.length) return;
        });
        
    }).catch(function(e:AxiosError){
        //res.send(e.message);
    });
}
function verifytoken(req,res,next){
    const bearerHeader=req.headers['authorization'];

    if(typeof bearerHeader !=='undefined'){
        const bearerToken=bearerHeader.split(" ")[1];

        req.jwt=bearerToken;

        next();
    }else{
        res.sendStatus(403) //ruta o acceso prohibido
    }
}

    


module.exports = router