"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var express = require('express');
var db = require('../db/index');
var database = require('../db/indexblockchain');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var BlockChain = require("../blockchain/blockChain");
var hash = require('object-hash');
var logger = fs.createWriteStream('./server/log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
});
function addLog(newlog) {
    logger.write(newlog + "\n");
}
var http = require('http');
var opcion;
var opciones = [];
var usuarios = [];
var ops = [];
var a;
//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/public.key', 'utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "3600s"
};
router.get('/listaelecciones', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var results, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.all()];
            case 1:
                results = _a.sent();
                res.json(results);
                return [3 /*break*/, 3];
            case 2:
                e_1 = _a.sent();
                console.log(e_1);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.get('/eleccion', verifytoken, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        jwt.verify(req.jwt, publicKey, verifyOptions, function (err) { return __awaiter(void 0, void 0, void 0, function () {
            var results, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!err) return [3 /*break*/, 1];
                        addLog(" >> Error en token login.");
                        res.error = true;
                        res.codigo = 403;
                        res.send(res);
                        return [3 /*break*/, 4];
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, db.all()];
                    case 2:
                        results = _a.sent();
                        res.json(results);
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        console.log(e_2);
                        res.sendStatus(500);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
router.get('/geteleccion/:id', verifytoken, function (req, res, next) {
    jwt.verify(req.jwt, publicKey, verifyOptions, function (err) { return __awaiter(void 0, void 0, void 0, function () {
        var results_1, ops_1, count, e_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!err) return [3 /*break*/, 1];
                    addLog(" >> Error en token login.");
                    res.error = true;
                    res.codigo = 403;
                    res.send(res);
                    return [3 /*break*/, 4];
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, db.one(req.params.id)];
                case 2:
                    results_1 = _a.sent();
                    ops_1 = [];
                    count = 0;
                    results_1.forEach(function (element) { return __awaiter(void 0, void 0, void 0, function () {
                        var e_4;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    return [4 /*yield*/, db.getCandidato(element.id_opcion)];
                                case 1:
                                    opcion = _a.sent();
                                    ops_1.push(opcion);
                                    count++;
                                    return [3 /*break*/, 3];
                                case 2:
                                    e_4 = _a.sent();
                                    console.log(e_4);
                                    res.sendStatus(500);
                                    return [3 /*break*/, 3];
                                case 3:
                                    if (count == results_1.length)
                                        res.json(ops_1);
                                    return [2 /*return*/];
                            }
                        });
                    }); });
                    return [3 /*break*/, 4];
                case 3:
                    e_3 = _a.sent();
                    console.log(e_3);
                    res.sendStatus(500);
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    }); });
});
router.post('/elecciones', verifytoken, function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        jwt.verify(req.jwt, publicKey, verifyOptions, function (err) { return __awaiter(void 0, void 0, void 0, function () {
            var op, jsonsalida, ideleccion_1, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!err) return [3 /*break*/, 1];
                        addLog(" >> Error en token login.");
                        res.error = true;
                        res.codigo = 403;
                        res.send(res);
                        return [3 /*break*/, 5];
                    case 1:
                        console.log("insert eleccion " + req.body.titulo);
                        op = req.body.opciones;
                        //id opcion nulo
                        op.push(7);
                        console.log(op);
                        jsonsalida = { 406: "error con comunicación a base de datos" };
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, db.insertarEleccion(req.body.titulo, req.body.descripcion, req.body.fecha_inicio, req.body.hora_inicio, req.body.fecha_fin, req.body.hora_fin, op)];
                    case 3:
                        ideleccion_1 = _a.sent();
                        req.body.opciones.forEach(function (element) {
                            console.log("insertar detalle eleccion ", ideleccion_1 + " " + element);
                            db.insertarDetalleEleccion(ideleccion_1, element);
                        });
                        jsonsalida = { "estado": 201, "id": ideleccion_1, "titulo": req.body.titulo, "descripcion": req.body.descripcion, "fecha_inicio": req.body.fecha_inicio, "hora_inicio": req.body.fecha_inicio, "fecha_fin": req.body.fecha_fin, "hora_fin": req.body.hora_inicio, "opciones": op };
                        res.json(jsonsalida);
                        addLog("eleccion insertardo:" + req.body.titulo + "\n");
                        return [3 /*break*/, 5];
                    case 4:
                        e_5 = _a.sent();
                        console.log(e_5);
                        res.sendStatus(500);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
router.post('/votacion', verifytoken, function (req, res, next) {
    console.log("jwt ", req.jwt);
    jwt.verify(req.jwt, publicKey, verifyOptions, function (err) { return __awaiter(void 0, void 0, void 0, function () {
        var jsonsalida, ideleccion, today, dateFrom, dateTo, timeFrom, timeTo, date, dateCheck, d1, t1, t, d2, t2, t3, c, ch, from, to, check, idvoto, blockChain, e_6;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!err) return [3 /*break*/, 1];
                    addLog(" >> Error en token login.");
                    res.error = true;
                    res.codigo = 403;
                    res.send(res);
                    return [3 /*break*/, 8];
                case 1:
                    jsonsalida = { 406: "error con comunicación a base de datos" };
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 7, , 8]);
                    return [4 /*yield*/, db.oneEleccion(req.body.ideleccion)];
                case 3:
                    ideleccion = _a.sent();
                    today = new Date();
                    dateFrom = ideleccion[0].fecha_inicio;
                    dateTo = ideleccion[0].fecha_fin;
                    timeFrom = ideleccion[0].hora_inicio;
                    timeTo = ideleccion[0].hora_fin;
                    console.log("hora i", timeFrom, "hora f ", timeTo);
                    date = (today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear();
                    dateCheck = date;
                    d1 = dateFrom.split("/");
                    t1 = timeFrom.split(":");
                    t = parseInt(t1[0]) * 60 + parseInt(t1[1]);
                    console.log(t1);
                    console.log(t);
                    d2 = dateTo.split("/");
                    t2 = timeTo.split(":");
                    t3 = parseInt(t2[0]) * 60 + parseInt(t2[1]);
                    console.log(t2);
                    console.log(t3);
                    c = dateCheck.split("/");
                    ch = today.getHours() * 60 + today.getMinutes();
                    console.log(ch);
                    from = new Date(d1[2], d1[0] - 1, d1[1]);
                    to = new Date(d2[2], d2[0] - 1, d2[1]);
                    check = new Date(parseInt(c[2]), parseInt(c[0]) - 1, parseInt(c[1]));
                    console.log("from ", from, "to ", to, "check ", check);
                    console.log(check > from && check < to);
                    console.log(check < from);
                    if (!((check > from && check < to) == false)) return [3 /*break*/, 4];
                    jsonsalida = { "estado": 404, "error": "eleccion no disponible" };
                    res.json(jsonsalida);
                    return [3 /*break*/, 6];
                case 4: return [4 /*yield*/, db.insertarVoto(req.body.cui, req.body.ideleccion, req.body.idopcion)];
                case 5:
                    idvoto = _a.sent();
                    console.log("id voto ", idvoto);
                    blockChain = new BlockChain();
                    blockChain.addNewTransaction(req.body.cui, req.body.ideleccion, req.body.idopcion);
                    blockChain.addNewBlock(null);
                    //console.log("Chain: ",blockChain.chain)
                    jsonsalida = { "estado": 201, "id": idvoto, "cui": req.body.Cui, "ideleccion": req.body.ideleccion, "idopcion": req.body.idopcion };
                    res.json(jsonsalida);
                    addLog("voto insertardo:" + idvoto + "\n");
                    _a.label = 6;
                case 6: return [3 /*break*/, 8];
                case 7:
                    e_6 = _a.sent();
                    console.log(e_6);
                    res.sendStatus(400);
                    return [3 /*break*/, 8];
                case 8: return [2 /*return*/];
            }
        });
    }); });
});
router.get('/resultado/:id', verifytoken, function (req, res, next) {
    console.log("jwt ", req.jwt);
    var ops = [];
    var id = req.params.id;
    jwt.verify(req.jwt, publicKey, verifyOptions, function (err) { return __awaiter(void 0, void 0, void 0, function () {
        var jsonsalida, results, e_7;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!err) return [3 /*break*/, 1];
                    addLog(" >> Error en token .");
                    res.error = true;
                    res.codigo = 403;
                    res.send(res);
                    return [3 /*break*/, 5];
                case 1:
                    jsonsalida = { 406: "error con comunicación a base de datos" };
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    console.log("id eleccion", req.params.id);
                    listaUsers(res, function () {
                        return __awaiter(this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2 /*return*/];
                            });
                        });
                    }.bind(this));
                    return [4 /*yield*/, db.allVotacionbyId(req.params.id)];
                case 3:
                    results = _a.sent();
                    addLog("get resultados votacion de la eleccion:" + req.params.id + "\n");
                    jsonsalida = { "Usuarios registrados": a, "votos": results };
                    res.json(jsonsalida);
                    return [3 /*break*/, 5];
                case 4:
                    e_7 = _a.sent();
                    console.log(e_7);
                    res.sendStatus(400);
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    }); });
});
router.get('/listaUsers', function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, axios.get('http://34.70.175.232:81/api/todos')
                    .then(function (response) {
                    var _this = this;
                    console.log("funcion then");
                    console.log("response: " + response.data);
                    var count = 0;
                    response.data.forEach(function (element) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            console.log("id opcion ", element.cui);
                            try {
                                console.log("entro al try ", count);
                                ops.push(element);
                                count++;
                            }
                            catch (e) {
                                console.log(e);
                                res.sendStatus(500);
                            }
                            if (count == response.data.length) {
                                a = response.data.length;
                                console.log("cantidad", a);
                                res.json(ops);
                            }
                            return [2 /*return*/];
                        });
                    }); });
                })["catch"](function (e) {
                    res.send(e.message);
                })];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
function listaUsers(res, callback) {
    axios.get('http://34.70.175.232:81/api/todos')
        .then(function (response) {
        var _this = this;
        console.log("funcion lista users");
        //console.log("response: "+response.data);
        var count = 0;
        response.data.forEach(function (element) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log("id opcion ", element.cui);
                try {
                    console.log("entro al try ", count);
                    ops.push(element.cui);
                    count++;
                }
                catch (e) {
                    console.log(e);
                    //res.sendStatus(500);    
                }
                console.log("ops ", ops);
                if (count == response.data.length)
                    return [2 /*return*/];
                return [2 /*return*/];
            });
        }); });
    })["catch"](function (e) {
        //res.send(e.message);
    });
}
function verifytoken(req, res, next) {
    var bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        var bearerToken = bearerHeader.split(" ")[1];
        req.jwt = bearerToken;
        next();
    }
    else {
        res.sendStatus(403); //ruta o acceso prohibido
    }
}
module.exports = router;
