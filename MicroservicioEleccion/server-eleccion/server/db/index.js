var axios = require('axios');
const mysql = require('mysql');

var fs = require('fs');

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}
//pool asdfasdf
const pool = mysql.createConnection({
    connectionLimit:10,
    host: 'db-mysql-eleccion',
    port: '3306',
    user: 'admin',
    password: 'root',
    database: 'prsa',
    multipleStatements : true
});

const con = mysql.createConnection({
    host: 'db-mysql-eleccion',
    port: '3306',
    user: 'admin',
    password: 'root',
    database: 'prsa'
});

let proyectobd = {};

proyectobd.all = () => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM eleccion;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });


};
proyectobd.one = (ideleccion) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM detalle_eleccion_opcion WHERE id_eleccion =" + ideleccion + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res);
        });
    });


};
proyectobd.oneEleccion = (ideleccion) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM eleccion WHERE ideleccion =" + ideleccion + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res);
        });
    });


};
proyectobd.getCandidato = (idopcion) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM candidato WHERE idcandidato =" + idopcion + ";";
        con.query(query, (err, res) => {
        if (err) throw err;
        
        //jsonsalida = {"id_candidato":res.idcandidato,"nombre":res.nombre,"id_partido":res.idpartido,};
        //console.log("getcandidato",jsonsalida);
        return resolve(res[0]);
        });
    });


};
proyectobd.insertarEleccion = (titulo,descripcion,fechai, horai,fechaf,horaf,opciones) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO eleccion(titulo,descripcion,fecha_inicio,hora_inicio,fecha_fin,hora_fin,opciones) VALUES ('" + titulo + "','" + descripcion + "','" + fechai + "','"+ horai +"','" + fechaf + "','"+ horaf +"','"+ opciones+"');";
        con.query(query, (err, res) => {
        if (err) throw err;           

        addLog("eleccion creada: "+res.insertId+"\n");
        
        resolve(res.insertId);
        });
    });


};
proyectobd.insertarDetalleEleccion = (id_eleccion,id_opcion) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO detalle_eleccion_opcion(id_eleccion,id_opcion) VALUES (" + id_eleccion + "," + id_opcion + ");";
        con.query(query, (err, res) => {
        if (err) throw err;           

        addLog("detalle eleccion creada: "+res.insertId+"\n");
        
        resolve(res.insertId);
        });
    });


};

proyectobd.insertarVoto = (idcui,ideleccion,idopcion) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO voto(cui,ideleccion,idopcion) VALUES (" + idcui + "," + ideleccion + "," + idopcion + ");";
        con.query(query, (err, res) => {
        if (err) throw err;           

        addLog("voto cread0: "+res.insertId+"\n");
        
        resolve(res.insertId);
        });
    });


};

proyectobd.allVotacionbyId = (ideleccion) => {
    //console.log("id elec ", ideleccion);
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM voto WHERE ideleccion=?`,[ideleccion], (err,results) =>{
            if(err){
                return reject(err);
            }
            return resolve(results);
        });
    });
};

module.exports = proyectobd;
