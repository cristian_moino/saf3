var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
// https://github.com/auth0/node-jsonwebtoken
var jwt = require('jsonwebtoken');
var app = express();
//********* LOG ***********************/
var logger = fs.createWriteStream('./server/log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
});
var today = new Date();
var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
function addLog(newlog) {
    logger.write(date + " " + time + " - " + newlog + "\n");
}
//********* BASE DE DATOS ***********************/
var mysql = require('mysql');
var con = mysql.createConnection({
    host: "db-mysql-token",
    port: 3306,
    user: "root",
    password: "root",
    database: "prsa"
});
con.connect(function (err) {
    if (err)
        console.log(err);
    console.log("Connected!");
});
//***********************************************/
var privateKey = fs.readFileSync('./server/private.key', 'utf8');
//var publicKey = fs.readFileSync('./public.key','utf8');
var payload = {};
console.log("Payload: " + JSON.stringify(payload));
/*
    Sign
*/
// Expiration timespan: https://github.com/auth0/node-jsonwebtoken#token-expiration-exp-claim
var exp = "3600s";
// JWT Token Options, see: https://tools.ietf.org/html/rfc7519#section-4.1 for the meaning of these
var signOptions = {
    expiresIn: exp,
    algorithm: "RS256" //algoritmo
};
console.log("Options: " + JSON.stringify(signOptions));
console.log("\n");
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
/** RUTA OBTENCION DE TOKEN */
app.post('/token', function (req, res) {
    var userpass = Buffer.from((req.headers.authorization || '').split(' ')[1] || '', 'base64').toString();
    var arrayDeCadenas = userpass.split(':');
    console.log(arrayDeCadenas[0]);
    console.log(arrayDeCadenas[1]);
    //console.log(req.query);
    addLog('>> Usuario que esta pidiendo un token: ' + arrayDeCadenas);
    var cadenaQuery = "select * from usuario where user='" + arrayDeCadenas[0] + "' and pass='" + arrayDeCadenas[1] + "';";
    var result = undefined;
    con.query(cadenaQuery, function (err, rows) {
        if (err) {
            console.log(err);
        }
        result = rows[0];
        //console.log(result.pass);
        if (result.pass == arrayDeCadenas[1]) {
            //console.log("entro al if", arrayDeCadenas)
            payload.valores = arrayDeCadenas;
            jwt.sign(payload, privateKey, signOptions, function (err, token) {
                addLog('>> Token que se enviara: ' + token);
                //console.log('>> Token que se enviara: '+token);
                addLog('>>');
                res.json({
                    token: token
                });
            });
        }
        else {
            res.writeHead(401, { 'Basic-Authenticate': 'Basic realm="nope"' });
            res.end('HTTP Error 401 Unauthorized: Access is denied');
            addLog('>> HTTP Error 401 Unauthorized: Access is denied');
        }
    });
});
app.post('/autenticacion', function (req, res, next) {
    console.log(req.body);
    addLog('>> Usuario que esta pidiendo un token: ' + req.body.email);
    var cadenaQuery = "select * from usuario where user='" + req.body.email + "' and pass='" + req.body.pass + "';";
    console.log(cadenaQuery);
    var result = undefined;
    con.query(cadenaQuery, function (err, rows) {
        if (err) {
            console.log(err);
        }
        result = rows[0];
        //console.log(result);
        if (result !== undefined) {
            //console.log("entro al if", arrayDeCadenas)
            payload.valores = result;
            jwt.sign(payload, privateKey, signOptions, function (err, token) {
                addLog('>> Token que se enviara: ' + token);
                //console.log('>> Token que se enviara: '+token);
                addLog('>>');
                res.json({
                    token: token
                });
            });
        }
        else {
            res.writeHead(401, { 'Basic-Authenticate': 'Basic realm="nope"' });
            res.end('HTTP Error 401 Unauthorized: Access is denied');
            addLog('>> HTTP Error 401 Unauthorized: Access is denied');
        }
    });
});
module.exports = app;
